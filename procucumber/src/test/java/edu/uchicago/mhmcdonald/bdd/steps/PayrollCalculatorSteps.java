package edu.uchicago.mhmcdonald.bdd.steps;

import edu.uchicago.mhmcdonald.bdd.proCucumber.PayrollCalculator;

import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import cucumber.api.PendingException;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class PayrollCalculatorSteps {

    private PayrollCalculator payrollcalculator;


    @Before
    public void setUp() {
        payrollcalculator = new PayrollCalculator();
    }

    @Given("^that my payroll calculator is available$")
    public void that_my_payroll_calculator_is_available() throws Throwable {
        assertNotNull(payrollcalculator);
    }


    @When("^I add the salary expense of (\\d+), (\\d+) and (\\d+)$")
    public void i_add_the_salary_expense_of_and(int arg1, int arg2, int arg3) throws Throwable {
        payrollcalculator.calculateTotalSalaries(5000, 3000, 7000);
    }

    @Then("^the salary expense should be \\$(\\d+)$")
    public void the_salary_expense_should_be_$(int salaryExpense) throws Throwable {
        assertEquals(salaryExpense, payrollcalculator.getSalaryExpense());
    }


}
