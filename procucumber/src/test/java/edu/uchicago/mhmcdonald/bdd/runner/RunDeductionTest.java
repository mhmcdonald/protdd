package edu.uchicago.mhmcdonald.bdd.runner;

/**
 * Created by markmcdonald on 8/10/16.
 */
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        format = { "pretty", "html:target/cucumber" },
        glue = "edu.uchicago.mhmcdonald.bdd.steps",
        features = {"classpath:cucumber/deduction.feature"}
)

//this class executes the steps performed in the DeductionCalculatorSteps located in the steps folder
public class RunDeductionTest {
}