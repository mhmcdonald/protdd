package edu.uchicago.mhmcdonald.bdd.runner;

/**
 * Created by markmcdonald on 8/10/16.
 */


import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        format = { "pretty", "html:target/cucumber" },
        glue = "edu.uchicago.mhmcdonald.bdd.steps",
        features = {"classpath:cucumber/payrollcalc.feature", "classpath:cucumber/deduction.feature"}
)
/*The purpose of this class is to have a comprehensive test suite that executes multiple features'
test scripts at one time. This is helpful in the event you want to display multiple tests for a single
Business user.*/
public class RunPayTestSuite1 {
}