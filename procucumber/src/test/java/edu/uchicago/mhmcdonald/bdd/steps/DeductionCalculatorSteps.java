package edu.uchicago.mhmcdonald.bdd.steps;

import edu.uchicago.mhmcdonald.bdd.proCucumber.PayrollCalculator;

import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by markmcdonald on 8/11/16.
 */
public class DeductionCalculatorSteps {

    private PayrollCalculator payrollcalculator;


    @Before
    public void setUp() {
        payrollcalculator = new PayrollCalculator();
    }


    @Given("^I am in a paycycle$")
    public void i_am_in_a_paycycle() throws Throwable {
        if (payrollcalculator.payCycle);
    }

    @When("^the current salary expense is \\$(\\d+)$")
    public void the_current_salary_expense_is_$(int arg1) throws Throwable {
        payrollcalculator.setSalaryExpense(15000);
    }

    @When("^I deduct \\$(\\d+) from the salary expense$")
    public void i_deduct_$_from_the_salary_expense(int deduction) throws Throwable {
        payrollcalculator.deductfromSalary(deduction);

    }

    @Then("^the salary expense with deductions should be \\$(\\d+)$")
    public void the_salary_expense_with_deductions_should_be_$(int salaryExpense) throws Throwable {
        assertEquals(salaryExpense, payrollcalculator.getSalaryExpense());

    }

}
