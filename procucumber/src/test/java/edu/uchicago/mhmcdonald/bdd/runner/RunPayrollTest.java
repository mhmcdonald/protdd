package edu.uchicago.mhmcdonald.bdd.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        format = { "pretty", "html:target/cucumber" },
        glue = "edu.uchicago.mhmcdonald.bdd.steps",
        features = {"classpath:cucumber/payrollcalc.feature"}
)

//this class executes the steps performed in the PayrollCalculatorSteps located in the steps folder
public class RunPayrollTest {
}
