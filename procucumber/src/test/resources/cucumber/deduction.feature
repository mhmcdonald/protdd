Feature: Deduction Calculator
  As an executive of this company, I need a payroll module that will calculate
  the cost of salaries with deductions removed from the overall expense so that I can
  know in real time what my payroll liability is

  Scenario: Deduct from salary expense for this month
    Given I am in a paycycle
    When the current salary expense is $15000
    And I deduct $400 from the salary expense
    Then the salary expense with deductions should be $14600