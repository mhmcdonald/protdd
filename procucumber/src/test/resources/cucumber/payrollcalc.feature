Feature: Payroll Calculator
  Behavior: As an executive of this company, my  payroll module will need to calculate
  the cost of salaries at my company, so that I can know in real time what my
  payroll liability is

  Scenario: Calculate salary expense for this month
    Given that my payroll calculator is available
    When I add the salary expense of 5000, 3000 and 7000
    Then the salary expense should be $15000
