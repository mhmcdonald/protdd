package edu.uchicago.mhmcdonald.bdd.proCucumber;


/*PayrollCaclculator is a simple java console program to calculate salary expenses, with deductions
for the purposes of a BDDCucumber testing tutorial */
public class PayrollCalculator {

    private int product;
    private  int salaryExpense;
    private int employeeCount;
    public boolean payCycle = true;

    //the following methods are called in the "steps" section of the test folder
    //the add function allows for the addition of three employee monthly salary amounts
    public void calculateTotalSalaries(int salary1, int salary2, int salary3) {
        salaryExpense = salary1 + salary2 + salary3;
    }

    //the deductfromSalary method allows for the reduction of salary expenses
    public void deductfromSalary(int deduction) {
        salaryExpense = salaryExpense - deduction;
    }

    //getter method to return the Salary expense
    public int getSalaryExpense() {
        return salaryExpense;
    }

    //setter method to set the Salary expenses
    public void setSalaryExpense(int amount) {
        salaryExpense = amount;
    }


}