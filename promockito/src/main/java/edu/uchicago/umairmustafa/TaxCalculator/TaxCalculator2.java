package edu.uchicago.umairmustafa.TaxCalculator;

public class TaxCalculator2 {
//this calculates the tax expense based on taxable income
    public double calculate(double taxableIncome) {
        if (taxableIncome > 500000) {
            return 50000 + ((taxableIncome - 500000) / 5);
        }
        return (taxableIncome / 10);
    }

    //REFACTORING
   /* public double calculate(double taxableIncome) {
        if(isIncomeIn20PercentTaxRange(taxableIncome)){
            return deduct20PercentAbove5Lacs(taxableIncome) + calculate(500000);
        }
        return (taxableIncome * 0.10);
    }

    private double deduct20PercentAbove5Lacs(double taxableIncome) {
        return (taxableIncome-500000)*.20;
    }

    private boolean isIncomeIn20PercentTaxRange(double taxableIncome){
        return taxableIncome > 500000;
    }
*/
}
