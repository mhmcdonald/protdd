package edu.uchicago.umairmustafa.Portfolio;

public interface StockService {
    //this returns the price of a stock
    public double getPrice(Stock stock);
}