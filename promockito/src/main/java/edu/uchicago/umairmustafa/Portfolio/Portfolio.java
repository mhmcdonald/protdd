package edu.uchicago.umairmustafa.Portfolio;

import java.util.List;

/**
 * this is an implmentation of a protfolio which contains a list of stocks and a stocks service
 the main functionality of this class is that it returns the value of portfolio
 */

public class Portfolio {
    private StockService stockService;
    private List<Stock> stocks;

    public void setStockService(StockService stockService) {
        this.stockService = stockService;
    }

    public void setStocks(List<Stock> stocks) {
        this.stocks = stocks;
    }

    /**
     * this method returns the value of portfolio
     */
    public double getMarketValue(){
        double marketValue = 0.0;

        for(Stock stock:stocks){
            try {
                for(int i = 0; i < stock.getQuantity(); i++){
                    marketValue += stockService.getPrice(stock);
                }
            }catch (Exception e){
                return -1;
            }
        }
        return marketValue;
    }
}