package edu.uchicago.umairmustafa.Portfolio;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;//Static imports allow you to access static fields and methods without specifying class name

public class PortfolioTester {

    private StockService stockService = Mockito.mock(StockService.class);//Mocks a Stock Service
    private Portfolio portfolio = new Portfolio();//Create a portfolio object which is to be tested

    @Test //ThenReturn method to mock behaviour of stock service for two stocks
    public void testThenReturn(){
        portfolio.setStockService(stockService);//set the stockService to the portfolio
        List<Stock> stocks = new ArrayList<Stock>();//Creates a list of stocks to be added to the portfolio
        Stock googleStock = new Stock("1","Google", 10);
        stocks.add(googleStock);
        portfolio.setStocks(stocks);//add stocks to the portfolio

        when(stockService.getPrice(googleStock)).thenReturn(50.00);

        double marketValue = portfolio.getMarketValue();
        Assert.assertEquals(500, marketValue, 0.0001);
    }

    @Test//ThenThrow method to handle exception on yahoo stock
    public void testThenThrow(){
        portfolio.setStockService(stockService);//set the stockService to the portfolio
        List<Stock> stocks = new ArrayList<Stock>();//Creates a list of stocks to be added to the portfolio
        Stock googleStock = new Stock("1","Google", 10);
        stocks.add(googleStock);
        portfolio.setStocks(stocks);//add stocks to the portfolio

        when(stockService.getPrice(googleStock)).thenThrow(new RuntimeException());

        double marketValue = portfolio.getMarketValue();
        Assert.assertEquals(-1, marketValue, 0.0001);
    }

    @Test//ThenAnswer behavior to show stateful behaviour on subsequent method calls
    public void testThenAnswer(){
        portfolio.setStockService(stockService);//set the stockService to the portfolio
        List<Stock> stocks = new ArrayList<Stock>();//Creates a list of stocks to be added to the portfolio
        Stock googleStock = new Stock("1","Google", 10);
        stocks.add(googleStock);
        portfolio.setStocks(stocks);//add stocks to the portfolio

        when(stockService.getPrice(googleStock)).thenAnswer(new Answer<Double>() {
            public double price = 40;

            @Override
            public Double answer(InvocationOnMock invocationOnMock) throws Throwable{
                price += 10;
                return price;
            }
        });

        double marketValue = portfolio.getMarketValue();
        Assert.assertEquals(950.0, marketValue, 0.0001);
    }

    @Test//Verify to test whether a method was called and number of calls to the method
    public void testMethodCalls(){
        portfolio.setStockService(stockService);//set the stockService to the portfolio

        List<Stock> stocks = new ArrayList<Stock>();//Creates a list of stocks to be added to the portfolio
        Stock googleStock = new Stock("1","Google", 10);
        stocks.add(googleStock);
        portfolio.setStocks(stocks);//add stocks to the portfolio

        double marketValue = portfolio.getMarketValue();
        verify(stockService, Mockito.times(10)).getPrice(googleStock);
    }

    @Test//Verify the arguments using matcher's
    public void testMethodArgumentsUsingMatcher(){
        portfolio.setStockService(stockService);//set the stockService to the portfolio

        List<Stock> stocks = new ArrayList<Stock>();//Creates a list of stocks to be added to the portfolio
        Stock googleStock = new Stock("1","Google", 1);
        stocks.add(googleStock);
        portfolio.setStocks(stocks);//add stocks to the portfolio

        double marketValue = portfolio.getMarketValue();
        verify(stockService).getPrice(Mockito.eq(googleStock));
    }
}