package edu.uchicago.umairmustafa.TaxCalculator;

import org.junit.Test;

import static org.junit.Assert.*;

public class TaxCalculator1Test {
    /**
     * JUnit test for verifying tax calculation for income less than 500,000
     */
    @Test
    public void when_income_less_than_5Lacs_then_deducts_10_percent_tax() {
        TaxCalculator1 taxCalculator = new TaxCalculator1();
        double payableTax = taxCalculator.calculate(400000.00);
        assertTrue(40000 == payableTax);
    }
    /**
     * JUnit test for verifying tax calculation for income greater than 500,000
     */
    @Test
    public void when_income_greater_than_5lacs_and_then_deducts_fifty_thousand_plus_20_percent_above_5lacs() {
        TaxCalculator1 taxCalculator = new TaxCalculator1();
        double payableTax = taxCalculator.calculate(800000.00);
        double expectedTaxForFirstFiveHundredThousand = 50000;
        double expectedTaxForReminder = 60000;
        double expectedTotalTax = expectedTaxForFirstFiveHundredThousand + expectedTaxForReminder;
        assertTrue(expectedTotalTax == payableTax);
    }
}